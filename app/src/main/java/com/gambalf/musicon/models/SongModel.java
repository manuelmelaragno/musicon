package com.gambalf.musicon.models;

import android.provider.MediaStore;
import android.util.Log;

public class SongModel {

    String id;
    String title;
    String album_id;
    String album_art;
    String artist;
    String path;
    String type;

    public SongModel() {
    }

    public SongModel(String id, String title, String album_id, String album_art, String artist, String path, String type) {
        this.id = id;
        this.title = title;
        this.album_id = album_id;
        this.album_art = album_art;
        this.artist = artist;
        this.path = path;
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getAlbum_id() {
        return album_id;
    }

    public void setAlbum_id(String album_id) {
        this.album_id = album_id;
    }

    public String getAlbum_art() {
        return album_art;
    }

    public void setAlbum_art(String album_art) {
        this.album_art = album_art;
    }

    public String getArtist() {
        return artist;
    }

    public void setArtist(String artist) {
        this.artist = artist;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
