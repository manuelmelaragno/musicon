package com.gambalf.musicon.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.gambalf.musicon.R;
import com.gambalf.musicon.models.SongModel;
import com.squareup.picasso.Picasso;

import java.io.Console;
import java.io.File;
import java.util.ArrayList;

public class SongAdapter extends ArrayAdapter<SongModel> {

    private ArrayList<SongModel> songs;
    private LayoutInflater inflater;
    private int Resource;
    Context context;

    public SongAdapter(Context context, int resource, ArrayList<SongModel> objects) {
        super(context, resource, objects);

        this.context = context;
        this.Resource = resource;
        this.songs = objects;
        inflater = (LayoutInflater.from(context));
    }

    private class ViewHolder{
        TextView title;
        TextView artist;
        TextView id;
        TextView path;
        ImageView albumArt;
        TextView type;
    }

    @NonNull
    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        ViewHolder viewHolder;

        if(convertView == null){
            convertView = inflater.inflate(Resource, null);
            viewHolder = new ViewHolder();
            viewHolder.title = (TextView)convertView.findViewById(R.id.itemTitleSong);
            viewHolder.artist = (TextView)convertView.findViewById(R.id.itemArtistSong);
            viewHolder.id = (TextView)convertView.findViewById(R.id.itemIdSong);
            viewHolder.path = (TextView)convertView.findViewById(R.id.itemPathSong);
            viewHolder.albumArt = (ImageView) convertView.findViewById(R.id.itemImgSong);
            viewHolder.type = (TextView) convertView.findViewById(R.id.itemTypeSong);

            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder)convertView.getTag();
        }

        viewHolder.title.setText(songs.get(position).getTitle());
        viewHolder.artist.setText(songs.get(position).getArtist());
        viewHolder.id.setText(songs.get(position).getId());
        viewHolder.path.setText(songs.get(position).getPath());
        viewHolder.type.setText(songs.get(position).getType());
        //viewHolder.title.setSelected(true);

        if(!songs.get(position).getAlbum_art().equals(""))
            Picasso.get().load(new File(songs.get(position).getAlbum_art())).into(viewHolder.albumArt);
        else
            Picasso.get().load(R.drawable.ic_song_deafult).into(viewHolder.albumArt);
        return convertView;
    }
}