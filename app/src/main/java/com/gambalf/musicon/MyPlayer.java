package com.gambalf.musicon;

import android.content.Context;
import android.media.AudioAttributes;
import android.media.AudioManager;
import android.media.MediaPlayer;
import android.os.Build;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;

import com.gambalf.musicon.models.SongModel;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class MyPlayer {

    //TODO Aggiungere istanza della SongsActivity per cambiare le icone della notifica

    public static TextView textViewControlsTitle = null;
    public static TextView textViewPlayerTitle = null;
    public static TextView textViewPlayerAlbumArtist = null;
    public static ImageView imageControlsCover = null;
    public static ImageView imagePlayerCover = null;
    public static PlayerActivity player = null;

    public static SongsActivity songsInstance;
    public static PlayerActivity playerInstance;

    public static ArrayList<SongModel> songs = new ArrayList<>();

    public static MediaPlayer mediaPlayer = new MediaPlayer();

    public static String curIdSong; //Current song id

    public MyPlayer(){
        mediaPlayer = new MediaPlayer();
        playerInstance = null;
    }

    static void playSong(String songUri){

        mediaPlayer.reset();


        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
            AudioManager am = (AudioManager) songsInstance.getApplicationContext().getSystemService(Context.AUDIO_SERVICE);
            am.requestAudioFocus(null, AudioManager.STREAM_MUSIC, AudioManager.AUDIOFOCUS_GAIN);

            mediaPlayer.setAudioAttributes(new AudioAttributes.Builder().setUsage(AudioAttributes.USAGE_MEDIA).build());
        }
        //mediaPlayer.setAudioStreamType(AudioManager.STREAM_MUSIC);

        try {
            mediaPlayer.setDataSource(songUri);
            mediaPlayer.prepare();
        } catch (IOException e) {
            e.printStackTrace();
        }
        mediaPlayer.start();
        SongsActivity.updateUI();
        if(playerInstance != null)
            playerInstance.updatePlayer();
        songsInstance.createNotification(false);

        if(SongsActivity.repeat.equals("ONE"))
            mediaPlayer.setLooping(true);
        else
            mediaPlayer.setLooping(false);

        mediaPlayer.setOnCompletionListener(new MediaPlayer.OnCompletionListener() {
            @Override
            public void onCompletion(MediaPlayer mp) {
                playNextSong();
            }
        });

    }

    static void playPrevSong(){

        for(int i=0;i<songs.size(); i++){

            if(i!=0&& curIdSong.equals(songs.get(i).getId())){

                SongsActivity.currentSongPath = songs.get(i-1).getPath();
                playSong(songs.get(i-1).getPath());

                curIdSong = songs.get(i-1).getId();

                if(textViewControlsTitle != null) {
                    textViewControlsTitle.setText(songs.get(i - 1).getTitle());
                    if(!songs.get(i - 1).getAlbum_art().equals(""))
                        Picasso.get().load(new File(songs.get(i - 1).getAlbum_art())).into(imageControlsCover);
                    else
                        Picasso.get().load(R.drawable.ic_song_deafult).into(imageControlsCover);
                }
                if(textViewPlayerTitle != null) {
                    textViewPlayerTitle.setText(songs.get(i - 1).getTitle());
                    textViewPlayerAlbumArtist.setText(songs.get(i - 1).getArtist());

                    if(!songs.get(i - 1).getAlbum_art().equals(""))
                        Picasso.get().load(new File(songs.get(i - 1).getAlbum_art())).into(imagePlayerCover);
                    else
                        Picasso.get().load(R.drawable.ic_song_deafult).into(imagePlayerCover);
                }

                if(player != null)
                    player.updatePlayer();

                break;
            }
        }

    }

    static boolean playPauseSong(){

        if(mediaPlayer.isPlaying()){
            mediaPlayer.pause();
            SongsActivity.updateUI();
            if(playerInstance != null)
                playerInstance.updatePlayer();
            songsInstance.createNotification(true);
            return true;
        }else {
            mediaPlayer.start();
            SongsActivity.updateUI();
            if(playerInstance != null)
                playerInstance.updatePlayer();
            songsInstance.createNotification(false);
            return false;
        }

    }

    static void playNextSong(){

        boolean ultima = false;

        for(int i=0;i<songs.size(); i++){

            if(i < (songs.size()-1) && curIdSong.equals(songs.get(i).getId())){

                SongsActivity.currentSongPath = songs.get(i+1).getPath();
                playSong(songs.get(i+1).getPath());

                curIdSong = songs.get(i+1).getId();

                if(textViewControlsTitle != null) {
                    textViewControlsTitle.setText(songs.get(i + 1).getTitle());
                    if(!songs.get(i + 1).getAlbum_art().equals(""))
                        Picasso.get().load(new File(songs.get(i + 1).getAlbum_art())).into(imageControlsCover);
                    else
                        Picasso.get().load(R.drawable.ic_song_deafult).into(imageControlsCover);
                }
                if(textViewPlayerTitle != null) {
                    textViewPlayerTitle.setText(songs.get(i + 1).getTitle());
                    textViewPlayerAlbumArtist.setText(songs.get(i + 1).getArtist());

                    if(!songs.get(i + 1).getAlbum_art().equals(""))
                        Picasso.get().load(new File(songs.get(i + 1).getAlbum_art())).into(imagePlayerCover);
                    else
                        Picasso.get().load(R.drawable.ic_song_deafult).into(imagePlayerCover);
                }

                if(player != null)
                    player.updatePlayer();

                break;
            }else if(i == (songs.size() - 1)){
                ultima = true;
            }
        }

        if(ultima && SongsActivity.repeat.equals("ALL")){
            SongsActivity.currentSongPath = songs.get(0).getPath();
            playSong(songs.get(0).getPath());

            curIdSong = songs.get(0).getId();

            if(textViewControlsTitle != null) {
                textViewControlsTitle.setText(songs.get(0).getTitle());
                if(!songs.get(0).getAlbum_art().equals(""))
                    Picasso.get().load(new File(songs.get(0).getAlbum_art())).into(imageControlsCover);
                else
                    Picasso.get().load(R.drawable.ic_song_deafult).into(imageControlsCover);
            }
            if(textViewPlayerTitle != null) {
                textViewPlayerTitle.setText(songs.get(0).getTitle());
                textViewPlayerAlbumArtist.setText(songs.get(0).getArtist());

                if(!songs.get(0).getAlbum_art().equals(""))
                    Picasso.get().load(new File(songs.get(0).getAlbum_art())).into(imagePlayerCover);
                else
                    Picasso.get().load(R.drawable.ic_song_deafult).into(imagePlayerCover);
            }

            if(player != null)
                player.updatePlayer();
        }

    }

    static SongModel getCurrentSong(String pathSong){

        SongModel currentSong = null;

        for(int i=0; i<songs.size(); i++){
            if(songs.get(i).getPath().equals(pathSong)){
                Log.d("DEBUGGINO", "Canzone trovata!");
                currentSong = songs.get(i);
                break;
            }
        }

        return currentSong;
    }

    static void setControlsViewsControls(TextView textViewControlsTitle, ImageView imageControlsCover ){
        MyPlayer.textViewControlsTitle = textViewControlsTitle;
        MyPlayer.imageControlsCover = imageControlsCover;
    }

    static void setControlsViewsPlayer(PlayerActivity player, TextView textViewPlayerTitle, TextView textViewPlayerAlbumArtist, ImageView imagePlayerCover ){
        MyPlayer.player = player;
        MyPlayer.textViewPlayerTitle = textViewPlayerTitle;
        MyPlayer.textViewPlayerAlbumArtist = textViewPlayerAlbumArtist;
        MyPlayer.imagePlayerCover = imagePlayerCover;
    }


}
