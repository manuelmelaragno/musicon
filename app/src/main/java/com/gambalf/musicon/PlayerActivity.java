package com.gambalf.musicon;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.os.Handler;
import android.support.constraint.ConstraintLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.util.Log;
import android.view.View;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.gambalf.musicon.models.SongModel;
import com.squareup.picasso.Picasso;

import java.io.File;

public class PlayerActivity extends AppCompatActivity {

    ConstraintLayout bgLayout;

    TextView textViewPlayerTitolo;
    TextView textViewPlayerAlbumArtist;
    TextView textViewPlayerTimeStart;
    TextView textViewPlayerTimeEnd;

    ImageButton btnPlayerPrev;
    ImageButton btnPlayerPlayPause;
    ImageButton btnPlayerNext;
    ImageButton btnRepeat;

    SeekBar seekBarPlayer;
    Handler mHandler = new Handler();

    ImageView imageViewPlayerCover;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_player);

        bgLayout = findViewById(R.id.bgPlayerLayout);

        textViewPlayerTitolo = findViewById(R.id.textViewPlayerTitolo);
        textViewPlayerAlbumArtist = findViewById(R.id.textViewPlayerAlbumArtist);
        textViewPlayerTimeStart = findViewById(R.id.textViewPlayerTimeStart);
        textViewPlayerTimeEnd = findViewById(R.id.textViewPlayerTimeEnd);

        btnPlayerPrev = findViewById(R.id.btnPlayerPrev);
        btnPlayerPlayPause = findViewById(R.id.btnPlayerPlayPause);
        btnPlayerNext = findViewById(R.id.btnPlayerNext);
        btnRepeat = findViewById(R.id.btnRepeat);

        seekBarPlayer = findViewById(R.id.seekBarPlayer);

        imageViewPlayerCover = findViewById(R.id.imageViewPlayerCover);

        MyPlayer.playerInstance = this;

        if(SongsActivity.repeat.equals("NONE")){ //SETTO L'ICONA DELLA RIPETIZIONE
            btnRepeat.setImageResource(R.drawable.ic_repeat_24dp);
        }else if(SongsActivity.repeat.equals("ONE")){
            btnRepeat.setImageResource(R.drawable.ic_repeat_one_green_24dp);
        }else{
            btnRepeat.setImageResource(R.drawable.ic_repeat_green_24dp);
        }

        btnRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(SongsActivity.repeat.equals("NONE")){ //SE NON E' ATTIVA LA RIPETIZIONE ALLORA ATTIVO LA RIPETIZIONE DI TUTTA LA LISTA
                    SongsActivity.repeat = "ALL";
                    MyPlayer.mediaPlayer.setLooping(false);
                    btnRepeat.setImageResource(R.drawable.ic_repeat_green_24dp);
                }else if(SongsActivity.repeat.equals("ALL")){ // SE E' ATTIVA LA RIPETIZIONE 'ALL' ALLORA ATTIVO LA RIPETIZIONE SINGOLA
                    SongsActivity.repeat = "ONE";
                    MyPlayer.mediaPlayer.setLooping(true);
                    btnRepeat.setImageResource(R.drawable.ic_repeat_one_green_24dp);
                }
                else if(SongsActivity.repeat.equals("ONE")){ // SE E' ATTIVA LA RIPETIZIONE 'ONE' ALLORA DISATTIVO TUTTO
                    SongsActivity.repeat = "NONE";
                    MyPlayer.mediaPlayer.setLooping(false);
                    btnRepeat.setImageResource(R.drawable.ic_repeat_24dp);
                }
            }
        });

        updatePlayer();
    }

    public void updatePlayer(){
        SongModel currentSong = MyPlayer.getCurrentSong(SongsActivity.currentSongPath);

        if(currentSong != null){

            if(!currentSong.getAlbum_art().equals("")) {
                Palette bgPalette = createPaletteSync(BitmapFactory.decodeFile(currentSong.getAlbum_art()));
                int dominantColor = bgPalette.getDominantColor(Color.parseColor("#000000"));
                GradientDrawable bgGradient = new GradientDrawable(GradientDrawable.Orientation.BOTTOM_TOP, new int[]{dominantColor, Color.parseColor("#3d3d3d")});
                bgGradient.setCornerRadius(0f);
                bgGradient.setDither(true);
                bgLayout.setBackground(bgGradient);
            }else{
                bgLayout.setBackground(getResources().getDrawable(R.drawable.gradient_background));
            }
            String duration = Integer.toString(MyPlayer.mediaPlayer.getDuration() / 1000 / 60) + ":"+Integer.toString((MyPlayer.mediaPlayer.getDuration() / 1000) % 60);

            textViewPlayerTitolo.setText(currentSong.getTitle());
            textViewPlayerAlbumArtist.setText(currentSong.getArtist());
            textViewPlayerTimeEnd.setText(duration);

            if(MyPlayer.mediaPlayer.isPlaying()) { //SETTO L'ICONA DELLA PAUSA/PLAY
                btnPlayerPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);
            }
            else {
                btnPlayerPlayPause.setImageResource(R.drawable.ic_play_circle_outline);
            }

            if(!currentSong.getAlbum_art().equals(""))
                Picasso.get().load(new File(currentSong.getAlbum_art())).into(imageViewPlayerCover);
            else
                Picasso.get().load(R.drawable.ic_song_deafult).into(imageViewPlayerCover);

            initSeekBar();

            initButtons();

            MyPlayer.setControlsViewsPlayer(this, textViewPlayerTitolo, textViewPlayerAlbumArtist, imageViewPlayerCover);
        }

        Log.d("DEBUGGINO", "Player aggiornato");
    }

    public void initButtons(){
        btnPlayerPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyPlayer.playPrevSong();
                btnPlayerPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);
            }
        });

        btnPlayerPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MyPlayer.playPauseSong()){ //True se viene messa in pausa la canzone, false altrimenti
                    btnPlayerPlayPause.setImageResource(R.drawable.ic_play_circle_outline);
                }else{
                    btnPlayerPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);
                }
            }
        });

        btnPlayerNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyPlayer.playNextSong();
                btnPlayerPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);
            }
        });
    }

    public void initSeekBar(){

        seekBarPlayer.setMax(MyPlayer.mediaPlayer.getDuration());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                seekBarPlayer.setProgress(MyPlayer.mediaPlayer.getCurrentPosition());
                mHandler.postDelayed(this, 1000);
            }
        });

        seekBarPlayer.getThumb().setAlpha(0);

        seekBarPlayer.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {

                String minutes = Integer.toString(MyPlayer.mediaPlayer.getCurrentPosition() / 1000 / 60);
                String seconds = Integer.toString((MyPlayer.mediaPlayer.getCurrentPosition() / 1000) % 60);

                if(fromUser)
                    MyPlayer.mediaPlayer.seekTo(progress);

                if((MyPlayer.mediaPlayer.getCurrentPosition() / 1000) % 60 < 10){
                    seconds = "0"+Integer.toString((MyPlayer.mediaPlayer.getCurrentPosition() / 1000) % 60);
                }

                textViewPlayerTimeStart.setText( minutes + ":"+ seconds);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                MyPlayer.mediaPlayer.pause();
                seekBarPlayer.getThumb().setAlpha(255);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                MyPlayer.mediaPlayer.start();
                seekBarPlayer.getThumb().setAlpha(0);
            }
        });
    }

    @Override
    public void onBackPressed() {
        MyPlayer.setControlsViewsPlayer(null, null,null,null);
        SongsActivity.updateUI();
        super.onBackPressed();
    }

    public Palette createPaletteSync(Bitmap bitmap) {
        Palette p = Palette.from(bitmap).generate();
        return p;
    }
}
