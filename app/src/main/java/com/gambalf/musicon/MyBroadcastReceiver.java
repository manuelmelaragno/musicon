package com.gambalf.musicon;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.media.AudioManager;
import android.util.Log;
import android.widget.Toast;

public class MyBroadcastReceiver extends BroadcastReceiver {

    private static final String TAG = "MyBroadcastReceiver";

    @Override
    public void onReceive(Context context, Intent intent) {
        /*StringBuilder sb = new StringBuilder();
        sb.append("Action: " + intent.getAction() + "\n");
        sb.append("URI: " + intent.toUri(Intent.URI_INTENT_SCHEME).toString() + "\n");
        String log = sb.toString();
        Log.d(TAG, log);
        Toast.makeText(context, log, Toast.LENGTH_LONG).show();*/

        if(intent.getAction().equals("Previous")){
            MyPlayer.playPrevSong();
        }else if(intent.getAction().equals("Pause")){
            if(MyPlayer.playPauseSong()){

            }
        }else if(intent.getAction().equals("Next")){
            MyPlayer.playNextSong();
        }
        if (intent.getAction().equals(AudioManager.ACTION_AUDIO_BECOMING_NOISY)) {
            Log.d("DEBUGGINO", "Cuffie Staccate");
            if(MyPlayer.mediaPlayer.isPlaying())
                MyPlayer.playPauseSong();
        }

    }
}