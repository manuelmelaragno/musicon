package com.gambalf.musicon;

import android.Manifest;
import android.annotation.TargetApi;
import android.app.ActivityManager;
import android.app.Notification;
import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.ContentResolver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.PixelFormat;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.media.AudioManager;
import android.media.browse.MediaBrowser;
import android.net.Uri;
import android.os.Build;
import android.os.Handler;
import android.provider.MediaStore;
import android.support.constraint.ConstraintLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.NotificationManagerCompat;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.SeekBar;
import android.widget.TextView;

import com.gambalf.musicon.adapters.SongAdapter;
import com.gambalf.musicon.models.SongModel;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class SongsActivity extends AppCompatActivity {

    private static final int MY_PERMISSIONS_REQUEST_READ_EXTERNAL = 1;
    private static final String CHANNEL_ID = "";
    private DrawerLayout mDrawerLayout;
    public static String currentSongPath;  //TODO : AGGIORNARE IL PATH DELLA CANZONE QUANDO IL PLAYER FINISCE DI RIPRODURLA E VA A QUELLA DOPO
    public static String repeat = "NONE"; //NONE - no repeat, ALL - Repeat the current list, ONE - repeat the current song

    //Main Layout
    LinearLayout mainLayout;

    //Controls
    ConstraintLayout controlsLayout;
    TextView txtViewControlsTitle;
    static ImageButton btnPrev, btnPlayPause, btnNext;
    ImageView imageViewControlsCover;
    static SeekBar seekBar;
    static Handler mHandler = new Handler();

    //Songs

    public ListView songsListView;
    public ListView albumsListView;
    public ListView albumsSongsListView;
    public ListView artistsListView;

    ArrayList<SongModel> albumsSongs;
    ArrayList<SongModel> albumsInMemory;
    ArrayList<SongModel> artistsInMemory;
    ArrayList<SongModel> songs;

    SongAdapter songAdapter;
    SongAdapter albumsAdapter;
    SongAdapter albumsSongsAdapter;
    SongAdapter artistsAdapter;

    //Notification
    NotificationManagerCompat notificationManager;
    public static Notification notification;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        MyPlayer.songsInstance = this;

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs);

        mainLayout = findViewById(R.id.mainLayout);
        mainLayout.getBackground().setDither(true);
        getWindow().setFormat(PixelFormat.RGBA_8888);

        setTitle("Songs");

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionbar = getSupportActionBar();
        actionbar.setDisplayHomeAsUpEnabled(true);
        actionbar.setHomeAsUpIndicator(R.drawable.ic_menu_black_24dp);

        mDrawerLayout = findViewById(R.id.drawer_layout);

        NavigationView navigationView = findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(
                new NavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(MenuItem menuItem) {

                        // close drawer when item is tapped
                        mDrawerLayout.closeDrawers();

                        // Add code here to update the UI based on the item selected
                        // For example, swap UI fragments here

                        if(menuItem.getItemId() == R.id.menuSongs && !menuItem.isChecked()){
                            loadSongs(-1);
                        }else if(menuItem.getItemId() == R.id.menuAlbums){
                            loadAlbums();
                        }

                        // set item as selected to persist highlight
                        menuItem.setChecked(true);
                        return true;
                    }
                });

        //Initialize Control buttons

        controlsLayout = findViewById(R.id.controlsLayout);
        txtViewControlsTitle = findViewById(R.id.textViewControlsTitle);
        imageViewControlsCover = findViewById(R.id.imageViewControlsCover);

        txtViewControlsTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SongsActivity.this, PlayerActivity.class);

                startActivity(intent);
            }
        });

        imageViewControlsCover.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SongsActivity.this, PlayerActivity.class);

                startActivity(intent);
            }
        });

        //btnPrev = findViewById(R.id.prevButton);

        /*btnPrev.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyPlayer.playPrevSong(txtViewControlsTitle);
                btnPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);
            }
        });*/

        btnPlayPause = findViewById(R.id.playPauseButton);

        btnPlayPause.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(MyPlayer.playPauseSong()){ //True se viene messa in pausa la canzone, false altrimenti
                    btnPlayPause.setImageResource(R.drawable.ic_play_circle_outline);
                }else{
                    btnPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);
                }
            }
        });

        btnNext = findViewById(R.id.nextButton);

        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                MyPlayer.playNextSong();
                btnPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);
            }
        });

        seekBar = findViewById(R.id.seekBar);


        seekBar.getThumb().setAlpha(0);

        seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                if(fromUser)
                    MyPlayer.mediaPlayer.seekTo(progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                seekBar.getThumb().setAlpha(255);
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                seekBar.getThumb().setAlpha(0);
            }
        });

        MyPlayer.setControlsViewsControls(txtViewControlsTitle, imageViewControlsCover);

        //Initialize list and adapter

        songsListView = findViewById(R.id.songsListView);
        albumsListView = findViewById(R.id.albumsListView);
        albumsSongsListView = findViewById(R.id.albumsSongsListView);
        artistsListView = findViewById(R.id.artistsListView);
        songs = new ArrayList<>();
        albumsInMemory = new ArrayList<>();
        albumsSongs = new ArrayList<>();
        artistsInMemory = new ArrayList<>();

        songAdapter = new SongAdapter(getApplicationContext(), R.layout.item_song, songs);
        albumsAdapter = new SongAdapter(getApplicationContext(), R.layout.item_song, albumsInMemory);
        albumsSongsAdapter = new SongAdapter(getApplicationContext(), R.layout.item_song, albumsSongs);
        artistsAdapter = new SongAdapter(getApplicationContext(), R.layout.item_song, artistsInMemory);
        songsListView.setAdapter(songAdapter);
        albumsListView.setAdapter(albumsAdapter);
        albumsSongsListView.setAdapter(albumsSongsAdapter);
        artistsListView.setAdapter(artistsAdapter);

        //Click on a song
        songsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                String itemType = ((TextView) view.findViewById(R.id.itemTypeSong)).getText().toString();

                if(itemType.equals("Song")) {
                    MyPlayer.songs = songs;
                    MyPlayer.curIdSong = ((TextView) view.findViewById(R.id.itemIdSong)).getText().toString();

                    String songPath = ((TextView) view.findViewById(R.id.itemPathSong)).getText().toString();
                    currentSongPath = songPath;
                    MyPlayer.playSong(songPath);

                    if(controlsLayout.getVisibility() == View.GONE)
                        controlsLayout.setVisibility(View.VISIBLE);

                    txtViewControlsTitle.setText(((TextView) view.findViewById(R.id.itemTitleSong)).getText().toString());

                    Drawable cover = ((ImageView)view.findViewById(R.id.itemImgSong)).getDrawable();
                    imageViewControlsCover.setImageDrawable(cover);

                    btnPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);



                    seekBar.setMax(MyPlayer.mediaPlayer.getDuration());
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            seekBar.setProgress(MyPlayer.mediaPlayer.getCurrentPosition());
                            mHandler.postDelayed(this, 1000);
                        }
                    });

                    createNotification(false);
                }
            }
        });


        //Click on a album
        albumsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Log.d("DEBUGGINO", "Clicco su un album");
                int albumId = Integer.parseInt(((TextView) view.findViewById(R.id.itemPathSong)).getText().toString());

                loadSongs(albumId);
            }
        });

        //Click on a song in an album list
        albumsSongsListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                String itemType = ((TextView) view.findViewById(R.id.itemTypeSong)).getText().toString();

                if(itemType.equals("Song")) {
                    MyPlayer.songs = albumsSongs;
                    MyPlayer.curIdSong = ((TextView) view.findViewById(R.id.itemIdSong)).getText().toString();

                    String songPath = ((TextView) view.findViewById(R.id.itemPathSong)).getText().toString();
                    currentSongPath = songPath;

                    Log.d("DEBUGGINO", currentSongPath);
                    MyPlayer.playSong(songPath);

                    if(controlsLayout.getVisibility() == View.GONE)
                        controlsLayout.setVisibility(View.VISIBLE);

                    txtViewControlsTitle.setText(((TextView) view.findViewById(R.id.itemTitleSong)).getText().toString());

                    Drawable cover = ((ImageView)view.findViewById(R.id.itemImgSong)).getDrawable();
                    imageViewControlsCover.setImageDrawable(cover);

                    btnPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);



                    seekBar.setMax(MyPlayer.mediaPlayer.getDuration());
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            seekBar.setProgress(MyPlayer.mediaPlayer.getCurrentPosition());
                            mHandler.postDelayed(this, 1000);
                        }
                    });

                    createNotification(false);
                    /*MyPlayer.songs = albumsSongs;
                    MyPlayer.curIdSong = ((TextView) view.findViewById(R.id.itemIdSong)).getText().toString();

                    String songPath = ((TextView) view.findViewById(R.id.itemPathSong)).getText().toString();
                    MyPlayer.playSong(songPath);

                    txtViewControlsTitle.setText(((TextView) view.findViewById(R.id.itemTitleSong)).getText().toString());
                    currentSongPath = ((TextView) view.findViewById(R.id.itemPathSong)).getText().toString();

                    Drawable cover = ((ImageView)view.findViewById(R.id.itemImgSong)).getDrawable();
                    imageViewControlsCover.setImageDrawable(cover);

                    btnPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);

                    seekBar.setMax(MyPlayer.mediaPlayer.getDuration());
                    mHandler.post(new Runnable() {
                        @Override
                        public void run() {
                            seekBar.setProgress(MyPlayer.mediaPlayer.getCurrentPosition());
                            mHandler.postDelayed(this, 1000);
                        }
                    });*/
                }
            }
        });

        getExternalPermissions(); //Controllo i permessi e carico le canzoni

        IntentFilter intentFilter = new IntentFilter(AudioManager.ACTION_AUDIO_BECOMING_NOISY);
        MyBroadcastReceiver myBroadcastReceiver = new MyBroadcastReceiver();
        registerReceiver(myBroadcastReceiver, intentFilter);

    }

    private void getExternalPermissions() {
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {

            // Permission is not granted
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, MY_PERMISSIONS_REQUEST_READ_EXTERNAL);
        }else{
            loadAlbums();
            loadSongs(-1);
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_EXTERNAL: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0  && grantResults[0] == PackageManager.PERMISSION_GRANTED) { //Permissions granted

                    loadAlbums();
                    loadSongs(-1);

                } else {
                    getExternalPermissions();
                }
                return;
            }
        }
    }

    public void loadSongs(int albumId) {
        setTitle("Songs");

        //songs.clear();
        //songAdapter.notifyDataSetChanged();

        if(albumId == -1) {
            if(songs.isEmpty()) {
                songs.clear();

                songAdapter.notifyDataSetChanged();
                getListSongs(albumId);
            }

            songsListView.setVisibility(View.VISIBLE);
            albumsListView.setVisibility(View.GONE);
            albumsSongsListView.setVisibility(View.GONE);
            artistsListView.setVisibility(View.GONE);
        }
        else if(albumId > -1){
            albumsSongs.clear();
            albumsSongsAdapter.notifyDataSetChanged();

            getListSongs(albumId);

            songsListView.setVisibility(View.GONE);
            albumsListView.setVisibility(View.GONE);
            albumsSongsListView.setVisibility(View.VISIBLE);
            artistsListView.setVisibility(View.GONE);
        }

    }

    private void getListSongs(int albumId) {

        ContentResolver cr = this.getContentResolver();

        Cursor cur = null;
        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String selection = MediaStore.Audio.Media.IS_MUSIC;
        String sortOrder = MediaStore.Audio.Media.DATE_ADDED + " ASC";
        if(albumId == -1)
            cur = cr.query(uri, null, selection, null, sortOrder);
        if(albumId > -1)
            cur = cr.query(uri, null, selection +" and album_id = " + albumId, null, sortOrder);

        int count = 0;

        if(cur != null)
        {
            count = cur.getCount();

            if(count > 0)
            {
                Log.d("MusicONDebug", "Count = " + Integer.toString(count));
                while(cur.moveToNext())
                {

                    //GET SINGLE SONG INFO
                    SongModel song = new SongModel();
                    song.setId(Integer.toString(cur.getInt(cur.getColumnIndex(MediaStore.Audio.Media._ID)))); //Setto l'id della riga
                    song.setPath(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.DATA))); //Setto il percorso della canzone
                    song.setTitle(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.TITLE))); //Setto il titolo della canzone
                    song.setAlbum_id(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))); //Setto l'id dell'album
                    song.setArtist(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST)) + " • " + cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM))); //Setto l'artista e l'album della canzone
                    song.setType("Song"); //Setto il tipo della riga

                    if(albumId > -1)
                        setTitle(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM)));

                    ContentResolver cr1 = this.getContentResolver();

                    Uri uri1 = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
                    Cursor cur1 = cr1.query(uri1, null, null, null, null);

                    if(cur1 != null){
                        while(cur1.moveToNext())
                        {
                            if(cur1.getInt(cur1.getColumnIndex(MediaStore.Audio.Albums._ID)) == Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID)))) {
                                if(cur1.getString(cur1.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART)) != null)
                                    song.setAlbum_art(cur1.getString(cur1.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART)));
                                else
                                    song.setAlbum_art("");
                            }
                        }
                    }

                    cur1.close();

                    if(albumId == -1)
                        songs.add(song);
                    else
                        albumsSongs.add(song);
                }

            }
        }

        cur.close();
        if(albumId == -1)
            songAdapter.notifyDataSetChanged();
        else
            albumsSongsAdapter.notifyDataSetChanged();
    }

    public void loadAlbums(){ //Load Albums
        setTitle("Albums");

        if(albumsInMemory.isEmpty()) {
            getListAlbum();
        }
        songsListView.setVisibility(View.GONE);
        albumsListView.setVisibility(View.VISIBLE);
        albumsSongsListView.setVisibility(View.GONE);
        artistsListView.setVisibility(View.GONE);
    }

    private void getListAlbum() {
        ContentResolver cr = this.getContentResolver();

        String album_name = "";

        Uri uri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;
        String projection[]  = new String[]{MediaStore.Audio.Media._ID, MediaStore.Audio.Media.ALBUM, MediaStore.Audio.Media.ALBUM_ID, MediaStore.Audio.Media.ARTIST};
        String sortOrder = MediaStore.Audio.Media.ALBUM + " COLLATE NOCASE ASC";
        Cursor cur = cr.query(uri, projection, null, null, MediaStore.Audio.Media.ARTIST);

        int count = 0;

        if(cur != null)
        {
            count = cur.getCount();

            if(count > 0)
            {
                Log.d("MusicONDebug", "Count = " + Integer.toString(count));
                while(cur.moveToNext())
                {
                    if(!album_name.equals(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM)))) {
                        //GET SINGLE SONG INFO
                        SongModel song = new SongModel();
                        song.setId(Integer.toString(cur.getInt(cur.getColumnIndex(MediaStore.Audio.Media._ID)))); //Setto l'id della riga
                        song.setPath(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM_ID))); //Setto l'id dell'album
                        song.setTitle(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM))); //Setto il nome dell'album
                        song.setArtist(cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ARTIST))); //Setto l'artista dell'album
                        song.setType("Album"); //Setto il tipo di item

                        ContentResolver cr1 = this.getContentResolver();

                        Uri uri1 = MediaStore.Audio.Albums.EXTERNAL_CONTENT_URI;
                        Cursor cur1 = cr1.query(uri1, null, null, null, null);

                        if (cur1 != null) {
                            while (cur1.moveToNext()) {
                                if (cur1.getInt(cur1.getColumnIndex(MediaStore.Audio.Albums._ID)) == Integer.parseInt(cur.getString(cur.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ID)))) {
                                    if(cur1.getString(cur1.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART)) != null)
                                        song.setAlbum_art(cur1.getString(cur1.getColumnIndex(MediaStore.Audio.Albums.ALBUM_ART)));
                                    else
                                        song.setAlbum_art("");
                                }
                            }
                        }

                        cur1.close();

                        boolean existed = false;
                        for(int i = 0; i<albumsInMemory.size(); i++){
                            if(albumsInMemory.get(i).getTitle().equals(song.getTitle()))
                                existed = true;
                        }
                        if(!existed)
                            albumsInMemory.add(song);

                        album_name = cur.getString(cur.getColumnIndex(MediaStore.Audio.Media.ALBUM));
                    }
                }

            }
        }

        cur.close();
        albumsAdapter.notifyDataSetChanged();
    }

    //NOTIFICATION
    @TargetApi(Build.VERSION_CODES.O)
    public void createNotification(Boolean paused){

        createNotificationChannel();

        // Create an explicit intent for an Activity in your app
        Intent intent = new Intent(this, PlayerActivity.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_REORDER_TO_FRONT);
        PendingIntent pendingIntent = PendingIntent.getActivity(this, 0, intent, 0);

        Intent prevIntent = new Intent(this, MyBroadcastReceiver.class);
        prevIntent.setAction("Previous");
        prevIntent.putExtra("ID Notifica", 1111);
        PendingIntent prevPendingIntent = PendingIntent.getBroadcast(this, 0, prevIntent, 0);

        Intent pauseIntent = new Intent(this, MyBroadcastReceiver.class);
        pauseIntent.setAction("Pause");
        pauseIntent.putExtra("ID Notifica", 1111);
        PendingIntent pausePendingIntent = PendingIntent.getBroadcast(this, 0, pauseIntent, 0);

        Intent nextIntent = new Intent(this, MyBroadcastReceiver.class);
        nextIntent.setAction("Next");
        nextIntent.putExtra("ID Notifica", 1111);
        PendingIntent nextPendingIntent = PendingIntent.getBroadcast(this, 0, nextIntent, 0);

        Bitmap notificationArt;
        if(!MyPlayer.getCurrentSong(currentSongPath).getAlbum_art().equals(""))
            notificationArt = BitmapFactory.decodeFile(MyPlayer.getCurrentSong(currentSongPath).getAlbum_art());
        else {
            notificationArt = BitmapFactory.decodeResource(getApplicationContext().getResources(), R.drawable.ic_song_deafult);
            Log.d("DEBUGGINO", "No Icon");
        }

        if(!paused) {
            notification = new Notification.Builder(this, CHANNEL_ID)
                    // Show controls on lock screen even when user hides sensitive content.
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setSmallIcon(R.drawable.ic_music_note_black)
                    // Add media control buttons that invoke intents in your media service
                    .addAction(R.drawable.ic_skip_previous, "Previous", prevPendingIntent) // #0
                    .addAction(R.drawable.ic_pause_black_48dp, "Pause", pausePendingIntent)  // #1
                    .addAction(R.drawable.ic_skip_next, "Next", nextPendingIntent)     // #2
                    // Apply the media style template
                    .setStyle(new Notification.MediaStyle()
                            .setShowActionsInCompactView(1, 2 /* #1: pause button */))
                    .setContentTitle(MyPlayer.getCurrentSong(currentSongPath).getTitle())
                    .setContentText(MyPlayer.getCurrentSong(currentSongPath).getArtist())
                    .setLargeIcon(notificationArt)
                    .setContentIntent(pendingIntent)
                    .setOngoing(true)
                    .build();
        }else{ //CANZONE IN PAUSA
            notification = new Notification.Builder(this, CHANNEL_ID)
                    // Show controls on lock screen even when user hides sensitive content.
                    .setVisibility(Notification.VISIBILITY_PUBLIC)
                    .setSmallIcon(R.drawable.ic_music_note_black)
                    // Add media control buttons that invoke intents in your media service
                    .addAction(R.drawable.ic_skip_previous, "Previous", prevPendingIntent) // #0
                    .addAction(R.drawable.ic_play_arrow_black_48dp, "Play", pausePendingIntent)  // #1
                    .addAction(R.drawable.ic_skip_next, "Next", nextPendingIntent)     // #2
                    // Apply the media style template
                    .setStyle(new Notification.MediaStyle()
                            .setShowActionsInCompactView(1, 2 /* #1: pause button */))
                    .setContentTitle(MyPlayer.getCurrentSong(currentSongPath).getTitle())
                    .setContentText(MyPlayer.getCurrentSong(currentSongPath).getArtist())
                    .setLargeIcon(notificationArt)
                    .setContentIntent(pendingIntent)
                    .setOngoing(false)
                    .build();
        }
        notificationManager = NotificationManagerCompat.from(this);

// notificationId is a unique int for each notification that you must define
        notificationManager.notify(1111, notification);
    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_LOW;
            NotificationChannel channel = new NotificationChannel(CHANNEL_ID, name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }

    public static void updateUI(){
        if(MyPlayer.mediaPlayer.isPlaying()) {
            btnPlayPause.setImageResource(R.drawable.ic_pause_circle_outline);
        }
        else {
            btnPlayPause.setImageResource(R.drawable.ic_play_circle_outline);
        }

        seekBar.setMax(MyPlayer.mediaPlayer.getDuration());
        mHandler.post(new Runnable() {
            @Override
            public void run() {
                seekBar.setProgress(MyPlayer.mediaPlayer.getCurrentPosition());
                mHandler.postDelayed(this, 1000);
            }
        });

    }

    @Override
    public void onBackPressed() {
        if(mDrawerLayout.isDrawerOpen(GravityCompat.START)){
            mDrawerLayout.closeDrawers();
        }else{
            //super.onBackPressed();
            this.moveTaskToBack(true);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                mDrawerLayout.openDrawer(GravityCompat.START);
                return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    protected void onDestroy() {
        notificationManager.cancel(1111);
        notificationManager.cancelAll();
        super.onDestroy();
    }

    @Override
    protected void onStop() {
        if(!isAppRunning()){
            notificationManager.cancel(1111);
            notificationManager.cancelAll();
        }
        super.onStop();
    }

    private boolean isAppRunning() {
        ActivityManager m = (ActivityManager) this.getSystemService( ACTIVITY_SERVICE );
        List<ActivityManager.RunningTaskInfo> runningTaskInfoList =  m.getRunningTasks(10);
        Iterator<ActivityManager.RunningTaskInfo> itr = runningTaskInfoList.iterator();
        int n=0;
        while(itr.hasNext()){
            n++;
            itr.next();
        }
        if(n==1){ // App is killed
            return false;
        }

        return true; // App is in background or foreground
    }
}
